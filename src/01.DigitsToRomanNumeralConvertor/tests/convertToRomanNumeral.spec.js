import convertFunctions from '../convertToRomanNumeral';
import each from 'jest-each';

describe('Convert to Roman Numberical', () => {
    
    const cases = [
        [1, 'I'],
        [5, 'V'],
        [10, 'X'],
        [50, 'L'],
        [100, 'C'],
        [500, 'D'],
        [1000, 'M'],
        [1980, 'MCMLXXX'],
        [531, 'DXXXI'],
        [3274, 'MMMCCLXXIV']
    ];

    each(cases).test(`should convert %d number to roman value of %s`, (input, expectedValue) => {
            expect(convertFunctions.convertDigitToRomanNumeral(input)).toEqual(expectedValue); 
        });

    each(cases).test(`should convert %d number to roman value of %s`, (input, expectedValue) => {
        expect(convertFunctions.convertDigitToRomanNumeralCurrying(input)).toEqual(expectedValue); 
    });
});
