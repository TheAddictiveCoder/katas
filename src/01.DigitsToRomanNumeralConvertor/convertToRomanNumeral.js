const romanLibrary = [
    [1000, 'M'],
    [900, 'CM'],
    [500, 'D'],
    [400, 'CD'],
    [100, 'C'],
    [90, 'XC'],
    [50, 'L'],
    [40, 'XL'],
    [10, 'X'],
    [5, 'V'],
    [4, 'IV'],
    [1, 'I'],
];

const convertDigitToRomanNumeral = input => {
    let romanCharacters = '';
    let leftOverValue = input;
    
    romanLibrary.forEach(element => {
        if(leftOverValue === 0)
            return;

        const divisionTimes = Math.floor(leftOverValue / element[0]);
        const remainder = leftOverValue % element[0]
        leftOverValue = remainder;

        if(divisionTimes > 0) romanCharacters += element[1].repeat(divisionTimes);
    });

    return romanCharacters;
};

const convertDigitToRomanNumeralCurrying = input => {
    const indexLibrary = romanLibrary.findIndex(value => input >= value[0]);
    if(indexLibrary === -1) return '';

    const romanValue = romanLibrary[indexLibrary][1];
    const digitValue = romanLibrary[indexLibrary][0];

    const repeatTimes = Math.floor(input / digitValue);
    const remainder = input % digitValue;

    return romanValue.repeat(repeatTimes, romanValue) + convertDigitToRomanNumeralCurrying(remainder);
};

const convertFunctions = {
    convertDigitToRomanNumeral,
    convertDigitToRomanNumeralCurrying 
};
export default convertFunctions;